from django.contrib import admin
from .models.user import User
from .models.account import Account
from .models.bill import Bill
from .models.product import Product

admin.site.register(User)
admin.site.register(Account)
admin.site.register(Bill)
admin.site.register(Product)

