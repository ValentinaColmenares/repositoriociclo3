from django.db import models

class Product(models.Model):
    id_product = models.AutoField(primary_key=True)
    name = models.CharField('Name', max_length = 60)
    price = models.IntegerField(default=0)