from django.db import models
from .user import User

class Bill(models.Model):
    id_bill = models.AutoField(primary_key=True)
    date = models.DateTimeField()
    user = models.ForeignKey(User, related_name='bill', on_delete=models.CASCADE)